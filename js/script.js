var app = angular.module('myApp', []);
app.controller('dataCtrl', ['$scope', function ($scope) {
    $scope.show = function() {
            $scope.items = $scope.input.split(',').sort()
            }
    $scope.rearrange = function() {
            var sortedList = $scope.input.split(',').sort()
            var listLength = sortedList.length
            var nbCol = 3
            var nbRow = Math.ceil(listLength/3)

            if (listLength > nbCol) {
                var rearrangedList = [];
                var nbLastRow = listLength - (nbRow - 1) * nbCol
                var myArray = [];
                for (var i1 = 0; i1 < nbRow * nbLastRow; i1 = i1 + nbRow) {
                    myArray.push(sortedList.slice(i1, i1 + nbRow))
                }
                for (var i2 = nbRow * nbLastRow; i2 < listLength; i2 = i2 + nbRow -1) {
                    myArray.push(sortedList.slice(i2, i2 + nbRow - 1))
                }

                for (var colIndex = 0 ; colIndex < nbRow; colIndex++) {
                    for (var rowIndex = 0 ; rowIndex < nbCol; rowIndex++) {
                        if (myArray[rowIndex][colIndex] !== undefined) {
                            rearrangedList.push(myArray[rowIndex][colIndex])
                        }
                    }
                }
                $scope.items = rearrangedList
            } else {
                $scope.items = sortedList
            }
            }
}]);